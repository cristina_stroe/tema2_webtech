
const FIRST_NAME = "Andreea Cristina";
const LAST_NAME = "Stroe";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
    
function initCaching() {
    var obiect={};
    var cache={};
    cache.pageAccessCounter=function(parametru)
    {
        if(parametru==null)
        {
            if(obiect.hasOwnProperty('home')) 
            {
                obiect['home']=obiect['home']+1;
            }
            else
            {
                if(obiect.hasOwnProperty('home')==false)
                {
                    obiect['home']=1;
                }
            }
        }
        else
        {
             var pageSection=String(parametru).toLowerCase();
             if(obiect.hasOwnProperty(pageSection))
             {
                 obiect[pageSection]=obiect[pageSection]+1;
             }
             else
             {
                 obiect[pageSection]=1;  
             }
        }   
    };
    
    cache.getCache=function()
    {
        return obiect;
    };
    
    return cache;
}



module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

